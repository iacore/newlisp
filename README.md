# newerLISP

An attempt to bring newLISP up to date.

## To do

- [x]port the build system to Zig
    - [x] build on linux
    - [ ] build on other platforms
- [x] fix C UB bugs
- replace pcre with latest version
    - add pcre test, then change
- extract doc HTML into programmatic data
- add search to HTML docs
- add ports
- add scheme-like (read) and (write)


Known <del>bugs</del> feature

- fixnums overflow/underflow
    nl-math.c:345 underflow
    `result += number;`

## Contribute

Issue and pull requests are welcome.

To build, run `zig build`.  
To install, run `sudo zig build install -p /usr/local`.

## Link

- [newLISP unofficial git repo](https://github.com/kosh04/newlisp)
- newLISP http://www.newlisp.org/
- newLISP Forum http://www.newlispfanclub.alh.net/

## License

newLISP and Nuevatec are trademarks of Lutz Mueller.
All files are distribute by GPLv3. For more details,
see [doc/COPYING](https://github.com/kosh04/newlisp/blob/master/doc/COPYING) and [doc/License.html](https://rawgit.com/kosh04/newlisp/master/doc/License.html).
