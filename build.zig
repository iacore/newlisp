const std = @import("std");

const Features = struct {
    ffi: bool = true,
    readline: bool = false,
    library: bool = false,
};

fn config_(exe: *std.build.Step.Compile, opts: Features) void {
    const target = exe.target;

    exe.linkSystemLibrary("m");
    exe.defineCMacro("_FORTIFY_SOURCE", "2");
    exe.defineCMacro("SUPPORT_UTF8", "1");

    if (opts.library) {
        exe.defineCMacro("LIBRARY", "1");
        exe.addCSourceFiles(&.{"unix-lib.c"}, &.{});
    }

    if (opts.ffi) {
        exe.defineCMacro("FFI", "1");
        exe.linkSystemLibrary("ffi");
    }

    if (opts.readline) {
        exe.defineCMacro("READLINE", "1");
        exe.linkSystemLibrary("readline");
    }

    const target_arch = target.getCpuArch();
    if (target_arch.isAARCH64() or target_arch.isX86()) {
        exe.defineCMacro("NEWLISP64", "1");
    }

    if (target.isLinux())
        exe.defineCMacro("LINUX", "1");
    if (target.isDarwin()) {
        // untested
        exe.defineCMacro("MAC_OSX", "1");
    }
    if (target.isWindows()) {
        // untested
        exe.defineCMacro("WINDOWS", "1");
    }
    if (target.getCpuArch().isWasm()) {
        // untested
        for ([_][]const u8{
            "MAC_OSX",
            // "LIBRARY",
            "EMSCRIPTEN",
        }) |name|
            exe.defineCMacro(name, "1");
    }
    // not added:
    // -DCYGWIN
    // -DAIX
    // -D_BSD
    // -DKFREEBSD

    const sources = &.{
        "newlisp.c",
        "nl-symbol.c",
        "nl-math.c",
        "nl-list.c",
        "nl-liststr.c",
        "nl-string.c",
        "nl-filesys.c",
        "nl-sock.c",
        "nl-import.c",
        "nl-xml-json.c",
        "nl-web.c",
        "nl-matrix.c",
        "nl-debug.c",
        "nl-utf8.c",
        "pcre.c",
    };
    exe.addCSourceFiles(sources, &.{});
}

pub fn build(b: *std.Build) void {
    const target = b.standardTargetOptions(.{});

    const optimize = b.standardOptimizeOption(.{});

    const exe = b.addExecutable(.{
        .name = "newlisp",
        .target = target,
        .optimize = optimize,
    });
    if (exe.optimize != .Debug) {
        exe.strip = true;
        exe.pie = true;
    }
    config_(exe, .{
        .readline = true,
    });

    b.installArtifact(exe);

    const run_cmd = b.addRunArtifact(exe);

    run_cmd.step.dependOn(b.getInstallStep());

    if (b.args) |args| {
        run_cmd.addArgs(args);
    }

    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);

    const lib = b.addSharedLibrary(.{
        .name = "newlisp",
        .target = target,
        .optimize = optimize,
    });
    config_(lib, .{ .library = true });

    b.installArtifact(lib);

    install_other_files(b);

    const test_step = b.step("test", "Run unit tests");

    var last_step: ?*std.build.Step = null;
    for ([_][]const []const u8{
        &.{"qa-dot"},
        &.{"qa-specific-tests/qa-dictionary"},
        &.{"qa-specific-tests/qa-xml"},
        &.{"qa-specific-tests/qa-json"},
        &.{"qa-specific-tests/qa-setsig"},
        &.{"qa-specific-tests/qa-net"},
        &.{"qa-specific-tests/qa-net6"},
        &.{"qa-specific-tests/qa-cilk"},
        &.{"qa-specific-tests/qa-ref"},
        &.{"qa-specific-tests/qa-message"},
        &.{"qa-specific-tests/qa-win-dll"},
        &.{"qa-specific-tests/qa-blockmemory"},
        &.{"qa-specific-tests/qa-exception"},
        &.{"qa-specific-tests/qa-float"},
        &.{"qa-specific-tests/qa-foop"},
        &.{"qa-specific-tests/qa-local-domain"},
        &.{"qa-specific-tests/qa-inplace"},
        &.{"qa-specific-tests/qa-utf16path"},
        &.{"qa-specific-tests/qa-pipefork"},
        &.{"qa-specific-tests/qa-libffi"},
        &.{ "qa-specific-tests/qa-bigint", "10000" },
        &.{"qa-specific-tests/qa-longnum"},
        &.{ "qa-specific-tests/qa-factorfibo", "60" },
        &.{"qa-specific-tests/qa-bench"},
    }) |args| {
        const run_test = b.addRunArtifact(exe);
        run_test.setName(args[0]);
        run_test.addArgs(args);

        if (last_step) |step| run_test.step.dependOn(step);
        last_step = &run_test.step;
    }
    test_step.dependOn(last_step.?);
}

pub fn install_other_files(b: *std.Build) void {
    // todo: create symbolic link
    // install -m 755  newlisp $PREFIX/bin/newlisp-10.7.5
    // ln -s $PREFIX/bin/newlisp-10.7.5 $PREFIX/bin/newlisp

    b.installBinFile("util/newlispdoc", "newlispdoc");

    inline for (.{
        "util/syntax.cgi",
        "util/newlisp.vim",
        "modules/canvas.lsp",
        "modules/cgi.lsp",
        "modules/crypto.lsp",
        "modules/ftp.lsp",
        "modules/getopts.lsp",
        "modules/gsl.lsp",
        "modules/infix.lsp",
        "modules/mysql.lsp",
        "modules/odbc.lsp",
        "modules/plot.lsp",
        "modules/pop3.lsp",
        "modules/postgres.lsp",
        "modules/postscript.lsp",
        "modules/smtp.lsp",
        "modules/smtpx.lsp",
        "modules/sqlite3.lsp",
        "modules/stat.lsp",
        "modules/unix.lsp",
        "modules/xmlrpc-client.lsp",
        "modules/zlib.lsp",
    }) |file|
        b.installFile(file, "share/newlisp/" ++ file);

    inline for (.{
        "COPYING",
        "CREDITS",
        "newlisp_manual.html",
        "newlisp_index.html",
        "manual_frame.html",
        "CodePatterns.html",
        "newLISPdoc.html",
        "newLISP-10.7.5-Release.html",
    }) |file|
        b.installFile("doc/" ++ file, "share/doc/newlisp/" ++ file);

    b.installFile("doc/newlisp.1", "share/man/man1/newlisp.1");
    b.installFile("doc/newlispdoc.1", "share/man/man1/newlispdoc.1");
}
